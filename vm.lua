--[[ Example BasIL Virtual Machine -- Logan "CodeWeaver" Hall
This is a simple VM implementation of BasIL'0' in Lua 5.3, for demonstration/education purposes.
NOTE: Lua 5.3 or later is required to run this, due to the use of previously-nonstandard bitwise operators.

Syscalls supported:
0: Print the contents of Register 1
1: Read a line of input from the user, into Register 0
]]

--[[ Internal routines ]]
function big_endian(str)
	--[[ Casts a string as a multi-byte number in big-endian ]]
	if type(str)=='number' then return str end	-- Already number

	local int = 0
	for i=1, str:len() do
		local byte = str:sub(i,i):byte()
		int = int + (byte * 256^(str:len()-i))
	end

	return int
end

function ascii(int)
	--[[ Casts a number as a sequence of ASCII bytes (string) ]]
	if type(int)=='string' then return int end	-- Already string

	local bytes = {}
	while int > 0 do
		bytes[#bytes+1] = int & 0xFF
		int = int >> 8
	end

	return string.reverse(string.char(table.unpack(bytes)))
end

function SGN_reset()
	sgn_dest = false
	sgn_src = false
	sgn_flag = nil
end

function string.split(str, sep)
	--[[ Split a string at the first occurrence of `sep`; return each substring ]]
	local values = {}
	for s in string.gmatch(str, "([^"..sep.."]+)", 1) do values[#values+1] = s end
	return table.unpack(values)
end


--[[ Initialize BasIL program and VM state ]]
debug = false	-- For printing out the status of the VM during execution
reg = {}	-- Table of registers
sgn_dest= false	-- When `true`, interpret `dest` arguments as negative
sgn_src = false	-- When `true`, interpret `src` arguments as negative
sgn_flag= nil	--[[ Tracks under which condition the state of `SGN` should be reset
	nil	Don't reset
	[#]	Number of instructions before SGN is reset	
	'i'	Reset after next `JMP` ("inclusive")
	'e'	Reset *at* next `JMP` ("exclusive")
]]
skip = false	-- When `true`, skip the next instruction and make `false`
callstack = {}	-- Stack of subroutine call locations (for returning to)
syscall = {	-- System functions
[0] =
	function()	-- Print
		print(math.tointeger(reg[1]) or reg[1])
	end,
	function()	-- ReadLine
		local input = io.read()
		reg[0] = tonumber(input) or input
	end
}

-- Load BasIL program specified by command-line parameter
prog = io.open(arg[1], 'rb')


--[[ Process header (if any) ]]
if prog:read(5) == "BasIL" then
	-- Verify "flavor"
	local flavor = prog:read(1)
	if flavor ~= '0' then
		print("Warning: This VM is not designed to interpret BasIL'"..flavor.."'!")
		if flavor == '\0' then print("(Char '0' expected, not to be confused with a 'NUL' byte 0x0)") end
	end

	-- Parse and repeat header info to the user
	local version = prog:read(1):byte()
	print("Running BasIL'"..flavor.."' revision "..version.."...")

	local label = prog:read(1)
	if label ~= '\0' then	-- Check whether platform label is omitted or not
		label = label..prog:read(16)
		print('Label: "'..label..'"')
	end
else
	prog:seek('set')
end


--[[ Process program DATA ]]
local register = 16
while true do
	-- Parse tag
	local tag = prog:read(1):byte()
	local Type = tag >> 6
	local Size = tag & 0x3F

	-- Parse value
	local data
	if Type < 3 and Size == 0 then	-- Read NUL-terminated value
		data = ''
		while true do
			local char = prog:read(1)
			if char == '\0' then break
			else data = data..char
			end
		end
	else
		data = prog:read(Size)
	end

	-- Handle based on "Type"
	if Type == 0 then	-- Integer
		reg[register] = big_endian(data)
	elseif Type == 1 then	-- "Real"
		local real = tonumber(data)
		if real == nil then
			print("DATA: Unable to parse '"..data.."' as a real number!")
			exit(false)
		end
		reg[register] = real
	elseif Type == 2 then	-- String
		reg[register] = data
	end

	register = register + 1	-- Point to the next register to load DATA into

	if Type == 3 then	-- Register*
		if Size == 0 then break
		else register = big_endian(data)
		end
	end
end


--[[ Execute program TEXT ]]
-- List of Opcodes (for readability)
local op = {
	MOV = 0x0, ADD = 0x1, SUB = 0x2, MUL = 0x3, DIV = 0x4, REM = 0x5, POW = 0x6, SGN = 0x7,
	AND = 0x8, ORR = 0x9, XOR = 0xA, EQU = 0xB, GTE = 0xC, LTE = 0xD, JMP = 0xE, SYS = 0xF
}
-- Operations
local exec = {
[0] =
	function(dest,src)	-- MOV
		reg[dest] = src
	end,
	function(dest,src)	-- ADD
		local operand = reg[dest]
		reg[dest] = type(operand)=='number' and operand + big_endian(src) or operand .. ascii(src)
	end,
	function(dest,src)	-- SUB
		local operand = reg[dest]

		if type(operand)=='number' then
			reg[dest] = operand - big_endian(src)
		elseif type(src)=='number' then
			reg[dest] = src>=0 and operand:sub(1,src+1) or operand:sub(src+1,-1)
		else -- dest & src are strings
			reg[dest] = operand:gsub(src, '')
		end
	end,
	function(dest,src)	-- MUL
		local operand = reg[dest]
		
		if type(operand)=='number' then
			reg[dest] = operand * big_endian(src)
		elseif type(src)=='number' then
			reg[dest] = operand:rep(src)
		else -- dest & src are strings
			reg[dest] = src .. operand
		end
	end,
	function(dest,src)	-- DIV
		local operand = reg[dest]

		if type(operand)=='number' then
			reg[dest] = operand / big_endian(src)
		else
			src = ascii(src)
			local fragment = operand:split(src)
			-- The register index *could* be non-numeric, so we have to be type-safe...
			reg[type(dest)=='number' and dest+1 or dest..1] = fragment
			reg[dest] = operand:sub(operand:len() - fragment:len() + 1)
		end
	end,
	function(dest,src)	-- REM (MOD)
		local operand = reg[dest]

		if type(operand)=='number' then
			reg[dest] = operand / big_endian(src)
		else
			src = ascii(src)
			local fragment = select(-1, operand:split(src))
			-- The register index *could* be non-numeric, so we have to be type-safe...
			reg[type(dest)=='number' and dest+1 or dest..1] = fragment
			reg[dest] = operand:sub(1, operand:len() - fragment:len() + 1)
		end
	end,
	function(dest,src)	-- POW
		local operand = reg[dest]

		if type(operand)=='number' then
			reg[dest] = operand ^ big_endian(src)
		elseif src~='' then
			reg[dest] = select(2, operand:gsub(src,''))
		else
			reg[dest] = operand:len()
		end
	end,
	function()	-- SGN
		-- Special instruction, see below
	end,
	function(dest,src)	-- AND
		local operand = reg[dest]
		if type(operand)=='number' then
			reg[dest] = big_endian(operand) & big_endian(src)
		else
			reg[dest] = ascii(big_endian(operand) & big_endian(src))
		end
	end,
	function(dest,src)	-- ORR
		local operand = reg[dest]
		if type(operand)=='number' then
			reg[dest] = big_endian(operand) | big_endian(src)
		else
			reg[dest] = ascii(big_endian(operand) | big_endian(src))
		end
	end,
	function(dest,src)	-- XOR
		local operand = reg[dest]
		if type(operand)=='number' then
			reg[dest] = big_endian(operand) ~ big_endian(src)
		else
			reg[dest] = ascii(big_endian(operand) ~ big_endian(src))
		end
	end,
	function(dest,src)	-- EQU
		if big_endian(reg[dest]) == big_endian(src) then skip = true end
	end,
	function(dest,src)	-- GTE
		if big_endian(reg[dest]) >= big_endian(src) then skip = true end
	end,
	function(dest,src)	-- LTE
		if big_endian(reg[dest]) <= big_endian(src) then skip = true end
	end,
	function(call,offset)	-- JMP
		if call==1 and offset == 0 then -- Return
			prog:seek('set', callstack[#callstack])
			table.remove(callstack)
		else
			if call==1 then table.insert(callstack, prog:seek()) end	-- Save current instruction pointer on callstack
			prog:seek('set', offset)
		end
	end,
	function(_,id)	-- SYS
		syscall[id]()
	end
}

-- Execution loop
while true do
	-- Fetch & decode instruction
	local inst = prog:read(1) or os.exit(0)
	inst = inst:byte()

	local Width = inst & 0x03
	local Opcode = inst >> 4
	if skip then	-- Previous conditional instruction tells us to skip this next instruction
		skip = false
		if Opcode ~= op.SGN then prog:seek('cur',2^Width) end
		goto next
	end
	local DestType = inst & 0x08 == 0 and 0 or 1
	local SourceType = inst & 0x04 == 0 and 0 or 1


	-- Handle `SGN` instruction specially
	if Opcode == op.SGN then
		sgn_dest = DestType==1 and true or false
		sgn_src = SourceType==1 and true or false

		if Width == 1 then sgn_flag = 1
		elseif Width == 2 then sgn_flag = 'i'
		elseif Width == 3 then sgn_flag = 'e'
		end	-- Ignore Width == 0; gets reset at next `SGN` instruction

		goto next
	end

	if not sgn_flag then ;
	elseif type(sgn_flag)=='number' then
		if sgn_flag>1 then sgn_flag = sgn_flag - 1
		else SGN_reset()
		end
	elseif Opcode==op.JMP then
		if sgn_flag=='i' then sgn_flag = 1
		else SGN_reset()
		end
	end


	-- Fetch argument(s)
	local Dest, Src

	local len = 2^Width
	local args = big_endian(prog:read(len))
	if Opcode < op.JMP then
		Dest = args >> (len * 4)
		Src = args & (16^len - 1)

		-- Check signs
		if sgn_dest then Dest = -Dest end
		if sgn_src then Src = -Src end

		-- Interpret argument types
		if DestType == 1 then	-- Dest is pointer to real destination
			Dest = reg[Dest]
		end
		if SourceType == 1 then	-- Src is register
			Src = reg[Src]
		end
	else
		Dest = DestType
		Src = sgn_src and -args or args
	end


	-- Execute op
	exec[Opcode](Dest,Src)

	::next::	-- Label to skip execution since Lua has no `continue` :(
end

