### Simple BasIL Assembler/Disassembler -- Logan "CodeWeaver" Hall
# BasIL is intended as a compilation target. But for theoretical/testing purposes, hand-writing BasIL assembly is desirable.
# This is a tool to make simple translations between assembly pseudocode, and BasIL bytecode, which explores some basic
# abstractions that an assembly language might make (labels, macros, linking, etc).

# Here are the syntax rules understood by this assembler:
#	1) Instructions are named by their three-letter code (e.g. `MOV`), and separated by line
#	2) JMP labels are barewords surrounded by brackets (e.g. `[LOOP]`)
#	3) Arguments:
#		a) Ordered Dest, Src (where applicable)
#		b) Separated by whitespace (commas are allowed and stripped like comments)
#		c) Integer literals are supported, including hexadecimal format (beginning with `0x`)
#		d) Registers are integers with a `%` prepended (e.g. `%15`)
#		e) A destination register can be used like a pointer by instead prepending `*` (e.g. `*15`)
#		f) Chars/strings are surrounded by single-quotes
#		g) `JMP` has one bareword argument which refers to a label
#	4) Assembler directives/macros begin with a dot '.'
#	   Built-ins:
#		a) `.HEAD [char flavor] [int revision] [str(17) label]` generates the program header
#		b) `.DATA` indicates where the program DATA is
#			  i) `.reg [int]` indicates which register to start inserting DATA from
#			 ii) `.int [int]` loads an integer into the next register
#			iii) `.num [str]` loads a real number by its string representation
#			 iv) `.str [str]` loads a string
#		c) `.TEXT` indicates where the program TEXT is
#			  i) `.CALL [label]` inserts a `JMP` that pushes the callstack
#			 ii) `.RET` inserts a `JMP` which pops the callstack
#	4) Comments begin with `;` and last until the end of the line


### Initialize assembler
use strict;
use feature 'switch';
use POSIX qw(floor ceil);


### Load assembly program specified by command-line parameter
open(my $asm_file, '<', $ARGV[0]);
our $asm;	# Assembly code
our $bin='';	# Binary output
{
	local $/;
	$asm = <$asm_file>;
}

# Strip comments / artifacts
$asm =~ s/;.*$//g;	# Remove comments ;...
$asm =~ s/,//g;		# Remove commas
$asm =~ s/^$//g;	# Remove blank lines


### Generate header (if any)
if ($asm =~ m/.HEAD\s+'(?<flavor>.*?)'\s+(?<revision>\d+)\s+(?<label>'.*?'|0)?/) {
	$bin .= "BasIL$+{flavor}";
	$bin .= chr($+{revision});

	if ($+{label} eq "0" or $+{label} eq "''") {	# No label, insert NUL byte
		$bin .= "\0";
	}
	else {	# Append label, pad up to 16 bytes
		my $label = ($+{label} =~ s/^'(.*)'$/$1/r);
		$label = substr($label, 0, 16);	# Truncate if too long
		$label .= "\0" x (16 - length($label));
		$bin .= $label;
	}
}


### Produce DATA segment
if ($asm =~ m/.DATA(?<data>.*?)(?:.HEAD|.TEXT|\Z)/sm) {
	my %tags = (
		int => 0, num => 1, str => 2, reg => 3
	);

	my $data = $+{data};	# Preserve DATA segment
	while ($data =~ m/\.(?<tag>\w{3})\s+(?<val>\S+)$/) {
		my $tag = $+{tag};

		# Process value according to tag
		my $val = $+{val};
		my $Size;

		die('DATA cannot be assigned to registers below %1!') if ($tag eq 'reg' and $val < 0);
		if ($tag eq 'int' or $tag eq 'reg') {	# Integral argument
			$Size = ceil(log($val) / log(2));
			$val = pack('J>', $val);	# Change integral value to raw bytes
		}
		elsif ($tag eq 'num' or $tag eq 'str') {	# String argument ("real" numbers are in string representation)
			$Size = length($val);
		}

		# Make value NUL-terminated if exceeds 6-bit size
		if ($Size >= 64) {
			$Size = 0;
			$val .= "\0";
		}

		# Form tag byte
		$tag = $tags{$tag};
		$tag <<= 6;
		$tag |= $Size;
		$tag = chr($tag);

		# Push bytecode
		$bin .= "$tag$val";
	}
}

# Conclude DATA segment with magic tag
$bin .= "\xC0";


### Produce TEXT segment
if ($asm =~ m/.TEXT(?<text>.*?)(?:.HEAD|.DATA|\Z)/sm) {
	my %ops = (
		MOV=>0, ADD=>1, SUB=>2, MUL=>3, DIV=>4, REM=>5, POW=>6, SGN=>7,
		AND=>8, ORR=>9, XOR=>10, EQU=>11, GTE=>12, LTE=>13, JMP=>14, SYS=>15
	);
	my %labels;

	my $text = $+{text};	# Preserve TEXT segment
	while ($text =~ m/^\s*(?:(?<op>\w{3})\s+(?:(?<dest>\S+)\s+)?(?<src>\S+)|\[(?<label>\w+)\])\s*$/g) {
		# If this is a label, track label position
		if (my $label = $+{label}) {
			$labels{$label} = length($bin);
			next;
		}

		#local %+;	# Reset regex capture groups each instruction
		my ($op,$dest,$src) = ($+{op},$+{dest},$+{src});	# Preserve values

		# Handle op
		my $Opcode = $ops{$op} << 4;
		my $DestType = ($dest =~ s/^\*// ? 8 : 0);	# Check for, and remove, pointer sigil
		$dest =~ s/^%//;	# Remove register sigil if present (instead of pointer sigil)
		my $SourceType = ($src =~ s/^%// ? 4 : 0);	# Check for, and remove, register sigil

		# Handle argument(s)
		my $Width;
		my $args;
		if (defined $dest) {	# dest and src are each half of Width; take the larger of the two
			$Width = floor(log($dest>=$src ? $dest : $src) / log(16));
			given($Width) {
				when(0) { $args = chr($dest | $src); }
				when(1) { $args = pack('CC', ($dest,$src)); }
				when(2) { $args = pack('nn', ($dest,$src)); }
				when(3) { $args = pack('NN', ($dest,$src)); }
			}
		}
		else {	# Just src
			$Width = floor(log($src) / log(256));
			given($Width) {
				when(0) { $args = chr($src); }
				when(1) { $args = pack('n', $src); }
				when(2) { $args = pack('N', $src); }
				when(3) { $args = pack('Q>', $src); }
			}
		}

		# Push bytecode
		$bin .= chr($Opcode | $DestType | $SourceType | $Width);
		$bin .= $args;
	}
}


### Output (redirect to file as desired)
print("$bin\n");
