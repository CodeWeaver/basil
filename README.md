## BasIL - Basic Intermediate Language

"BasIL" (pronounced "basil") is an open bytecode format for unlikely compilation targets.

It is designed to be simple for novices to implement a virtual machine for, leveraging high-level features.
The intent of BasIL is to allow arbitrary code to be compiled for arbitrary target systems. A compiler back-end can be
painstakingly written *once* for a source language, and then developers can quickly roll out a virtual machine for
every environment they want to support. (It's like a RISC architecture, but designed for high-level, virtual runtimes
instead of CPUs.)

Examples where such a bytecode target could be useful:

* Writing in any language for some kind of embedded, interpreted system
* Compiling "upwards", from a low-level language (e.g. C++) to a high-level environment (e.g. Lua)
* Transpiling without actually transpiling (it's easier to compile for a VM than it is to transpile to another language)
* Academia -- learn how compilers, interpreters, & virtual machines work!


## Specification

### HEADER
```
Byte	Data
----	----
0..4	"BasIL" ; Magic number
5	"Flavor" identifier (ASCII; e.g. '0','F', ...)
6	Revision number (0..255)
7,23	ASCII identifier for intended target platform (omitted if byte 7 is NUL)
```
A BasIL program MAY begin with a header, either 8 or 24 bytes long (as specified above). If a BasIL program doesn't begin
with the magic number, a VM MUST assume the header is omitted, and begin execution at byte 0. If the header is present,
a VM MAY act on it arbitrarily, or ignore it altogether. BasIL VMs have the freedom to terminate or alter its execution
based on information in the header. VMs might also stipulate whether special information (e.g. arguments, configuration, etc)
can be encoded within the target identifier (bytes 7..23). The inclusion of a program header may allow VMs to
adjust themselves accordingly for different supported/unsupported programs, rather than risking unknown behavior.

Byte 5 designates what "flavor" of BasIL the program targets. "Flavors" are mutually-exclusive formats for BasIL programs,
designed for different kinds of VMs. A VM MUST support one or more BasIL "flavors". Recognized flavors so far:

* BasIL'0' -- The "standard" specification. The number might increase if the standard receives any major changes.
* BasIL'+' -- The standard spec, *plus* a full implementation of the (recommended, optional) BasIL Standard Library.
* BasIL'B' -- Fixed-field format, for VMs with no bitwise operations. All bitfields are given their own byte.
Bitwise instructions not allowed (VMs MAY panic).

More flavors may be added over time (to accomodate really unorthodox VMs), and flavors may be revised over time (hence
the revision number at Byte 6). Revisions should be, at least, backwards-compatible.

### DATA

The program body begins with the "DATA" segment, where literals (constants & globals) are listed. Literals are declared up-front
particularly when they are several-byte values, like strings and large integers. Small integers will generally be included in
the program text as immediate values. Values are preceded by a 1-byte "tag" describing how to read the value:
```
(big-end)
Type : 2 bits
	00 Integer
	01 Real number (ASCII)
	10 String (ASCII)
	11 Register*
Size : 6 bits (0..63 bytes)
(lil-end)
```
The VM loads each literal into concurrent virtual registers. BasIL is like a Harvard architecture; the program itself is
read-only, so data is brought into the VM's working memory: an infinite number of registers. But which register to start from?
By default, Register 16, saving 4-bit indices for local variables. However, a DATA tag of "Type" `Register*` is used to select
a register to start from. Several `Register*` tags allow the program to store different DATA in different regions.

An optimizing compiler would put the most frequently-accessed values into earlier registers, so that the program uses the most
"compact" instructions to access them. The earliest registers are expected to be reserved for local/temporary variables, which
are accessed often; however, DATA might be loaded before Register 16 in programs which rely on less than 16 registers for
transient use.

Each "tag" tells the VM how to read the next value. The "Size" field indicates how many bytes wide the value will be (not
counting the tag itself). In BasIL, integers are always read Big-Endian. However, DATA can also be "real" numbers (with
fractional parts), and strings, which are both read as ASCII. "Reals" are read as ASCII to allow programs and VMs to use
arbitrary formats; programs might choose to encode scientific notation, and VMs might internally represent reals as strings,
IEEE 754, fixed-point, or other.

Special behavior occurs when "Size" is `0`. If the "Type" is `Register*`, then this tag concludes the DATA segment. For all
other types, it means the value is NUL-terminated. This allows reals and strings (and integers??) to exceed 63 bytes.

BasIL instructions use integer arguments *only* (immediate values & register indices). BasIL VMs SHOULD support reals and
strings, but they MUST be loaded from the DATA segment. After this point, BasIL is type-agnostic and VMs must preserve/recast
register types as specified.

### TEXT

#### INSTRUCTIONS

Instructions are each 1-9 bytes long, depending on the format and operands described within the first byte:
```
(big-end)
Opcode : 4 bits
	0000 MOV (move/copy)
	0001 ADD (add/concatenate)
	0010 SUB (subtract/substring)
	0011 MUL (multiply)
	0100 DIV (divide)
	0101 REM (remainder)
	0110 POW (power)
	0111 SGN (sign)

	1000 AND (bitwise AND)
	1001 ORR (bitwise OR)
	1010 XOR (bitwise XOR)
	1011 EQU (branch if equal)
	1100 GTE (branch if greater-than)
	1101 LTE (branch if less-than)
	1110 JMP (jump/call/return)
	1111 SYS (syscall)
DestType : 1 bit
	0 register ; register `dest`
	1 pointer  ; register indexed by value in `dest`
SourceType : 1 bit
	0 register
	1 immediate value
Width : 2 bits
	00 One byte for arg(s)
	01 Two bytes for arg(s)
	10 Four bytes for arg(s)
	11 Eight bytes for arg(s)
(lil-end)
Dest ; Register index
Src  ; Register index
```
Each instruction performs some operation (specified by "Opcode") with two operands ("Dest", "Src"). "SourceType" identifies
whether `src` is an immediate value, or referring to a register. "DestType" specifies whether the target register is `dest`,
or *referenced by* `dest`. For example: If "DestType" is `pointer`, `dest` is Register 7, and the value of Register 7 is `43`,
then the actual target & destination will be Register 43. (If "DestType" is `pointer` but the value of `dest` is a non-integer,
the VM SHOULD find a non-clobbering register, e.g. a hash table.)

Most Opcodes are diadic and work as: `dest := dest (op) src`. The exceptions are:
```
MOV: dest := src
SGN: [no operands]
JMP: PC := arg
SYS: arg()
```
`JMP` and `SYS` only have one, full-width argument ("`arg`"), which is treated like "Src" (whereas "Dest" and "Src" each span
half of "Width" in other instructions).

#### TYPES

A quick reminder: BasIL code itself is type-agnostic, but BasIL registers support real numbers and strings. Instructions
handle values intuitively based on their type, as described below. Registers are not strongly-typed.

"DestType" and "SourceType" are not the same thing. These tell the VM how to interpret the instruction's arguments.

#### OPCODES

For all instructions with a "Dest" field, the result is computed as described below, and stored in the "Dest" register.

##### 0000 MOV
Move a value into a register. In the case "Src" is a register, this can be thought of as copying a value between registers.

##### 0001 ADD
Add a "Src" value to a "Dest" value. If "Dest" contains a string, "Src" is treated like a string and appended.

##### 0010 SUB
Subtraction. For numbers, subtract "Src" from "Dest". When "Dest" contains a string...
* If "Src" is a number, "Dest" will become a substring from [0..`src`]. Negative values count from the end.
* If "Src" is a string, all occurrences of "Src" will be removed from "Dest".

##### 0011 MUL
Multiply "Dest", "Src" times. When "Dest" is a string...
* If "Src" is a number, "Dest" will repeat itself "Src" times.
* If "Src" is a string, "Src" and "Dest" will be concatenated. This is the opposite order of `ADD`, which concatenates "Dest"
and "Src".

##### 0100 DIV
Divide "Dest" by "Src". If "Dest" is a string, "Src" is interpreted as a string, and "Dest" is divided at the *first*
occurrence of "Src". **The first "half" is stored in `dest+1` (the immediate next register).** The rest of the string remains
in `dest`. If "Src" is not found in "Dest", `dest+1` is set to an empty string.

##### 0101 REM
Get the remainder of "Dest" divided by "Src". If "Dest" is a string, "Src" is interpreted as a string, and "Dest" is divided at
the *last* occurrence of "Src". **The last "half" is stored in `dest+1` (the immediate next register).** The rest of the string
remains in `dest`. If "Src" is not found in "Dest", `dest+1` is set to an empty string.

##### 0110 POW
Take "Dest" to the power of "Src". If "Dest" is a string, "Src" is interpreted as a string, and the number of occurrences of
"Src" is counted. As a special case: If "Src" is an empty string, the result will be the length of "Dest".

##### 0111 SGN
This is a special instruction. BasIL interprets all instruction arguments as **unsigned.** `SGN` is used to signal the VM to
interpret further arguments as positive or negative.
```
(big-end)
SGN : 4 bits
DestType : 1 bit
	0 "Dest" is positive
	1 "Dest" is negative
SourceType : 1 bit
	0 "Src" is positive
	1 "Src" is negative
Width : 2 bits
	00 until next `SGN`
	01 for one instruction
	10 until next `JMP` (inclusive)
	11 until next `JMP` (exclusive)
(lil-end)
```
Keep in mind, arguments are *always* unsigned. This instruction tells the VM whether to negate "Dest" or "Src". This is
stateful; all arguments are interpreted as described here until the condition specified by "Width" is met. A "Width" of `10` or
`11` is appropriate to keep a `SGN` state "local" to the current subroutine, before jumping elsewhere in the code.

This can be used to pass negative immediate values, and even access "negative" registers, which is entirely valid. Registers 0
and below are guaranteed not to be loaded with DATA when the program begins.

##### 1000 AND
Perform a bitwise-AND on "Dest" and "Src".

All bitwise operations work the same for all types (strings are preserved and treated according to their ASCII values).

##### 1001 ORR
Perform a bitwise-OR on "Dest" and "Src".

##### 1010 XOR
Perform a bitwise-XOR on "Dest" and "Src".

##### 1011 EQU
Compare "Dest" and "Src"; if they are equal, skip the next instruction.

For conditional instructions (`EQU`, `GTE`, `LTE`), it is anticipated that the "next instruction" is a `JMP`.

##### 1100 GTE
Compare "Dest" and "Src"; if "Dest" is greater-than or equal to "Src", skip the next instruction. The next instruction will be
executed if "Dest" is less than "Src".

##### 1101 LTE
Compare "Dest" and "Src"; if "Dest" is less-than or equal to "Src", skip the next instruction. The next instruction will be
executed if "Dest" is greater than "Src".

##### 1110 JMP
"Jump" to another part of the program.

`JMP` has only a single argument, "Src". "Src" is an absolute offset which tells the VM which **byte** to jump to. Beware, it's
measured in bytes and *not* instructions, because BasIL instructions are variable-width.

This instruction also serves the functionality otherwise known as `CALL`, and `RET` ('return'). Since `JMP` has no "Dest"
field, this is what "DestType" is used for: When "DestType" is `1`, the jump is treated like a subroutine "call", and the
current position in the code is saved by the VM, to be revisited later. When "DestType" is `1` **and** "Src" is `0`, this is a
special case which means "return to the last call".

##### 1111 SYS
"Syscall" -- call upon a system function.

`SYS` is similar to `JMP`. They both have one argument ("Src"), and they're both used to call functions. However, `SYS` calls
functions that belong to the virtual machine itself. "Src" is used to identify which function to call.

System functions are strictly implementation-defined. The syscalls offered by one VM are probably not compatible with another;
code should be made to emit the appropriate syscalls for the target environment. To help mitigate this, the BasIL'+' spec
defines a "standard library" for VMs to strive to implement (discussed below).

#### CALLING CONVENTION

Most functions accept an arbitrary set of parameters, and return a value when finished. How does BasIL expect `JMP` and `SYS`
calls to pass this information?

Register 0 is reserved for return values. It is no coincidence that it cannot be preloaded with DATA, so that constant/static
data cannot be clobbered by function calls. Function parameters begin from Register 1. A well-written function should not need
too awfully many parameters, but it may be a good idea to start saving local temporaries from Register 15 downwards, to avoid
smashing the parameter list.

System functions (i.e. `SYS`) SHOULD NOT write to any registers other than Register 0, especially outside of the parameter list
(starting from Register 1). Any that do should explicitly document which registers are expected to be modified.

User subroutines (i.e. `JMP`) MIGHT write to the parameter list (starting from Register 1), but it is not recommended. Instead,
users/compilers should find a set of registers that aren't already being used up the call stack. One method is to select
registers that are very likely not in use, such as registers with negative indices, or string keys, though this might not be
the most optimized solution for subroutines which are called often.

User subroutines SHOULD preserve `SGN` state. When execution returns to the caller, the `SGN` should not be different from when
the call was made.

This is a "convention". All BasIL code and VMs SHOULD adhere to it, but there is no strict guarantee. Consult any necessary
documentation to confirm that certain functions work the way you expect them to.

### BasIL'+' STD

Any BasIL VM MUST implement all of the above information. They're also expected to expose a set of system functions that can
be called with `SYS`, but none in particular are required. Listed here are a set of "standard" functions that a VM MUST
implement, ONLY if it claims to support BasIL'+'. Otherwise, it is a suggestion of things any BasIL VM may want to expose.
Good candidates for syscalls are common operations that the VM natively supports, much faster than the user program.

Each entry below lists which identifier must be passed to `SYS` (e.g. "syscall 0" means `SYS` with an argument of `0`), what
type of value gets stored in Register 0 (if any; "void" means Register 0 is untouched, "any" means type-agnostic), and what
registers are read as function arguments.

#### I/O

##### **syscall 0**: void Print( Register 1 )
Display the contents of Register 1.

##### **syscall 1**: string ReadLine()
Get a line of input from STDIN, or wait for the user to enter one.

##### **syscall 2**: integer ReadKey()
Wait for the user to press a key (blocking, like `syscall 1`). Store the keycode in Register 0.

VMs are encouraged to expose non-blocking inputs through an implementation-defined, volatile register.

#### Math

##### **syscall 32**: number Root( Registers 1,2 )
Take Register 1 to the root of Register 2; e.g. to get the square root of 25, call `SYS 32` when Register 1 has `25` and
Register 2 has `2`.

##### **syscall 33**: real Random()
Get a random real number between 0 and 1.

#### Bit-level operations

##### **syscall 64**: any LeftShift( Registers 1,2 )
Logically shift the value in Register 1 by [Register 2] bits to the left.

##### **syscall 65**: any RightShift( Registers 1,2 )
Logically shift the value in Register 1 by [Register 2] bits to the right.

##### **syscall 66**: any LeftRotate( Registers 1,2 )
"Rotate" the value in Register 1 by [Register 2] bits to the left; wrap the highest bits around to the lowest bits.

##### **syscall 67**: any RightRotate( Registers 1,2 )
"Rotate" the value in Register 1 by [Register 2] bits to the right; wrap the lowest bits around to the highest bits.

#### String operations



#### Debugging




## Example

Here is a short BasIL program to exemplify the spec:
```
01000010 01100001 01110011 01001001 01001100 00110000 00000000 00000000
11000001 00001101 00000100 00010010 10111001 10110000 10100001 00000100
01111111 11111111 11111111 11111111 00000010 10111100 10001111 11000000
00110100 11011111 01010100 11011110 01110110 11100000 00000111
```
Keep in mind, if this were an actual binary program, any text editor would just show you incoherent text. For clarity, it's
written here as a binary sequence.

What does this program do? You could step through each byte and trace it yourself, but let's go ahead and disassemble it.
Shown below is an assembly language pseudocode for the above program:
```
.HEAD '0', 0, 0

.DATA
.reg 13
.int 314159265
.int 0x7FFFFFFF
.int 48271

.TEXT
MUL %13, %15
REM %13, %14
JMP -2
```
The three core sections are present: The header, the DATA, and the TEXT (program code). Our theoretical assembler generates a
header automatically, with flavor '0', revision number 0, and with no platform label (zeroed out). So, the first eight bytes
of the program are the header, which looks like this:
```
BasIL0..
```
The dots are non-printing `NUL` bytes.

The DATA segment begins with a directive to start loading values at Register 13. That is the `11000001 00001101` at the start
of the second row: "Set the register (`11`) to a one byte-wide index (`000001`) of 13 (`00001101`)". Then, three integer values
are loaded in succession; four bytes, four bytes, and two bytes wide, respectively. It is implied the assembler automatically
encodes their widths so that the assembly author does not need to. The next byte in the machine code reads `00000100`, which
tells BasIL to read an integer (`00`) from the next four bytes (`000100`); the next four bytes represent 314159265 in
Big-Endian. Another four-byte integer (`00000100`) is read; then a two-byte integer (`00000010`). The assembler implicitly
generates the byte `11000000` to signal the end of the DATA segment.

The last row of machine code is our program code, or "TEXT". According to our assembly, there are three instructions here:
First, we multiply Registers 13 and 15 together, and store the result in Register 13. Then, we get the remainder of Register 13
divided by Register 14, and put that result in Register 13. Then, we jump two instructions earlier, thereby looping the
program. *There is one problem here* which the assembler abstracts away: All TEXT values are *unsigned,* which means a single
`JMP` instruction cannot jump backwards on its own. Indeed -- in the machine code, the first four bytes belong to `MUL` and
`REM`, and the last two bytes belong to `JMP`, but there's one byte in between. This is a `SGN` instruction which tells BasIL
to interpret the next `JMP` offset as a negative value. So of course, that means the machine code really just says to jump...
**`7`**.

Don't forget, BasIL is a variable-width architecture. The assembler has made another abstraction: The programmer wants to jump
two instructions back, but the virtual machine running this program needs to know how many *bytes* to jump. `MUL` spans two
bytes, `REM` spans two bytes, and `SGN` is a single byte, but it also begins counting from after the `JMP` instruction, meaning
another two bytes in this case.

Does `SGN` *need* to be there, inside the program loop? *No!* It's the only way to represent a negative value in the program
TEXT, but we could also refer to a register containing a negative value. We could load a negative "real" number from the DATA
segment, or we could begin the program TEXT with a `MOV %12, -6`. This would still require the assembler to emit a `SGN`
instruction (followed by a `MOV %12, 6`), but this can occur *once* outside of the program "loop". Then, our `JMP` instruction
should look like `11100100 00001100`; the first byte means this is a `JMP` with a register argument, and the second byte refers
to Register 12. With these changes, here's what the program TEXT looks like instead:
```
01110101 00000000 11000101 00110100 11011111 01010100 11011110 11100100
00001100
```
Our program is now two bytes longer, because we added a `MOV` instruction. However, the looping part may run marginally faster,
now, especially if it were a CPU-intensive routine (as opposed to the `MUL` and `REM` we have).

Now we know what this program does. But what does it "do"? It's a "linear congruential generator", which is a very efficient,
and predictable, "random number generator". Obviously the numbers are very non-random, but this program implements a simple
formula which produces wildly-varying 32-bit numbers. The "seed" is stored in Register 13, the modulus in Register 14, and
the multiplier in Register 15. These constants were selected from C++11's `minstd_rand` function (according to Wikipedia), and
the initial seed, 314159265, was selected from Fortran's original `ranlux` generator (according to GNU.org). The value of
Register 13 is the "random" output, for any given period.


## Motivation

The original reason for designing BasIL is so that C++ code can be written for "fantasy consoles" like the PICO-8.
Fantasy consoles are made to resemble microcomputers of the '80s and impose artifical constraints on developers.
(It's a lot more fun than it sounds.) The vast majority of these natively run an interpreted language of some kind;
often Lua, sometimes JavaScript, sometimes BASIC, etc.

I began writing a small C++ game with the goal of making it as "portable" as possible. This is achieved by writing as much
"platform-independent" code as possible, thereby only rewriting a small "platform-dependent" namespace for each port.
In search of target systems to test my design, I [re-]discovered "fantasy consoles", and made a point to consider them
seriously.

This design pattern is already foresaken if I just re-write my game in Lua. The point is to not rewrite the "core" codebase.
In that case, how do you get C++ to run in a pure Lua environment? Transpilation came to mind first, but no such transpiler
exists (and I'm not about to pioneer it). Next, I imagined retargeting GCC to output Lua bytecode. That would not be helpful
in this case because fantasy consoles generally don't allow code deserialization (i.e. `loadstring()`). I would have to
write it myself.

Writing it myself became an exciting idea, and I get a kick out of a Lua bytecode interpreter written in Lua. But at that
point, why Lua's bytecode? I wouldn't be able to leverage things like closures & upvalues (C++ is already lower level than
that). What I really wanted to write was a *virtual machine* of some kind. It was all falling into place, I just had to
decide on a target architecture.

I initially narrowed my choices down to what GCC supports, and what seemed feasible/novel to implement. Fantasy consoles don't
permit terribly large programs, and I'm not a wizard, so RISCs (Reduced Instruction Set Computer) are the only real options.
AVR32 has a great condensed instruction format, but there are almost 128 opcodes; too much for me. ARM is incredibly simple,
but each chip is slightly different, so I couldn't be too sure what *exact* form GCC would spit out, and what my VM should
expect. And only then did it occur to me, writing an ARM emulator may or may not violate ARM's patents/copyrights. The two
open-source standards (RISC-V and OpenRISC) were also a little too complicated for me; they're probably wonderfully efficient
in the realm of CPUs, but this VM needs to be absolutely minimal.

At this point, I reconsidered the Lua bytecode, and decided that, no matter what, a bytecode designed for higher-level
consumption would be crucial. Lua's still seemed like overkill to me, though, so I looked around for other "general-purpose"
bytecode formats. It turns out, bytecode is everywhere, but it's all proprietary. CPython generates an intermediate format
unique to CPython. Any Lua interpreter is free to have its own, entirely-unique bytecode. The Java Virtual Machine is fairly
consistent, but unique to the Java programming language.

Cue the appropriate XKCD; I doubt that me inventing one more bytecode format is going to become some reigning standard, but
at any rate, I wanted to design my own, implement a GCC back-end for it, and then write a *simple* VM in all the fantasy
consoles: PICO-8, TIC-80, Homegirl, LIKO-12, and so on.
