.HEAD '0' 0 'Fibonacci seq'

.DATA
.reg 1
.str 'How many numbers should we generate? '	; %1: temporary storage
.int 0	; %2: First number of sequence
.int 1	; %3: Second number of sequence

.TEXT
JMP MAIN

[DEC]	; Count how many numbers to generate
SUB %0 1	; Decrement loop counter (from user input)
GTE %0 0	; End program if counter < 0
JMP EXIT
.RET

[MAIN]
SYS 0	; Prompt user for length of fibonacci sequence
SYS 1	; Get user's input

; Display first two numbers
.CALL DEC
MOV %1 %2
SYS 0
.CALL DEC
MOV %1 %3
SYS 0

[LOOP]
.CALL DEC

; Compute Fibonacci n
MOV %1 %2	; Move n-2 into %1
ADD %1 %3	; f(n) = n-2 + n-1
MOV %2 %3	; Move n-1 into %2
MOV %3 %1	; Move n into %3

SYS 0
JMP LOOP

[EXIT]
